package lk.ijse.absd.springhibernate;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class MyController {

    @GetMapping
    public String helloSpringWebMVC() {
        return "Hello Spring Web MVC1";
    }
}
