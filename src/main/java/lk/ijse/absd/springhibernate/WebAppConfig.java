package lk.ijse.absd.springhibernate;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@ComponentScan("lk.ijse.absd.springhibernate")
@Configuration
@EnableWebMvc
@Import(JPAConfig.class)
public class WebAppConfig {
}
