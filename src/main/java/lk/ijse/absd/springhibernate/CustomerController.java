package lk.ijse.absd.springhibernate;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@CrossOrigin //Cross Origin Error Fix
@RestController //@Controller + @ResponseBody
@RequestMapping("/customer")
public class CustomerController {

    @GetMapping
    public String getALlCustomers() {
        return "All Customers";
    }

    /*@GetMapping("/id") //this append id to the mapping "/customer" => /customer/id
    public String getCustomer() {
        return "Customers Available";
    }*/

    /*@GetMapping("/?") // Any thing can be appended => /customer/1 (But Only Single Character) can put ("/??") son on for the required characters
    public String getCustomer() {
        return "Customers Available";
    }*/

    /*@GetMapping("/a?") // customer/a1, customer/a2
    public String getCustomer() {
        return "Customers Available";
    }*/

    /*@GetMapping("/*") // customer/ab, customer/a2, customer/ab => /customer/{path segment}/ ex: /customer/ab/ = here ab is a path segment
    public String getCustomer() {
        return "Customers Available";
    }*/

    /*@GetMapping("/**") // Any number of path segments are considered => /customer/ab/bv/cd
    public String getCustomer() {
        return "Customers Available";
    }*/

    @GetMapping("/{id:C\\d{3}}") // same as /customer/* => but can access the value given for id => Can add Validation for id
    public String getCustomer(@PathVariable("id") String customerID) {
        return "Get a Customer" + customerID;
    }

    @GetMapping(params = {"sort", "filter"}) // Sending Query Parameters
    public String getCustomer() {
        return "Get Filtered Customers";
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE) //Specify the Media Type
    public String updateCustomer() {
        return "Customer Updated";
    }

    @PutMapping
    public String saveCustomer() {
        return "Customer Saved";
    }

    @DeleteMapping
    public String deleteCustomer() {
        return "Customer Deleted";
    }
}
