package lk.ijse.absd.springhibernate;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello2")
public class MyController2 {
    public String helloSpringWebMVC2() {
        return "Hello Spring Web MVC2";
    }
}
